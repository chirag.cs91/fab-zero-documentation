# Installing **WSL**  (Windows Sub System for Linux)

  Installing WSL allows you to create a lightweight enviroment  that allowd you to install and run supported version of Linux(such as Ubuntu, Debian, etc) without having to set up a Virtual Box or use another computer.  There  are two ways to install WSL to you windows laptop.

 1. How to install WSL using Settings [Windows Subsystem for Linux (WSL) on Windows 10](https://www.windowscentral.com/install-windows-subsystem-linux-windows-10)
 2. How to install Windows using [PowerShell or Command Prompt](https://docs.microsoft.com/en-us/windows/wsl/install).

 You can choose any of the the methods and any Linux system but we choose to use [Ubunto](https://ubuntu.com/)

 ### Problems You might face while installing Ubuntu like me 
   The problems I faced is shown in the picture below. The error was because I needed to update my wsl. 

   ![ ](./images/error.png)
   
I downloaded a WSL from [Link](https://wslstorestorage.blob.core.windows.net/wslblob/wsl_update_x64.msi) and installed. After which I again faced another problem shown below.
  ![ ](./images/error1.png)

  I followed the instruction given on the [link](https://askubuntu.com/questions/1275748/how-do-i-make-ubuntu-20-04-work-on-windows-10-wsl-2) and I was able to successfully open Ubunto.